﻿using Autofac;
using System.Windows;
using System.Windows.Data;
using WpfProgressBar.Converters;
using WpfProgressBar.Models.Mock;
using WpfProgressBar.ViewModels;

namespace WpfProgressBar
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        public static IContainer? Container { get; private set; }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            SetupAutofac();
        }

        private static void SetupAutofac()
        {
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterType<DownloadMe>().As<IDownloadMe>();
            containerBuilder.RegisterType<MainWindowModel>();
            containerBuilder.RegisterType<UnitsToTextConverter>().As<IMultiValueConverter>();

            Container = containerBuilder.Build();
        }
    }
}