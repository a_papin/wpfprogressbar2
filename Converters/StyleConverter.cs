﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace WpfProgressBar.Converters
{
    public class StyleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));
            
            var styleName = value as string;

            if (styleName == null)
                throw new ArgumentNullException(nameof(value));
            
            var newStyle = (Style)Application.Current.TryFindResource(styleName) 
                           ?? (Style)Application.Current.TryFindResource("MainStyle");

            return newStyle;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
