﻿using System;
using System.Threading;

namespace WpfProgressBar.Models.Mock
{
    public interface IDownloadMe
    {
        int TotalBytesToDownload { get; }

        event Action<int> BytesReceived;
        event Action Connected;
        event Action Connecting;
        event Action Finished;
        event Action Finishing;
        event Action Paused;

        void StartDownload(CancellationToken cancellationToken, int initialPosition = 0);
    }
}