﻿using Prism.Commands;
using Prism.Mvvm;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfProgressBar.Models.Mock;

namespace WpfProgressBar.ViewModels
{
    public class MainWindowModel : BindableBase
    {
        #region Private fields

        private int _progressValue;
        private int _totalProgress;
        private string? _state;
        private bool _isIndeterminate;
        private ICommand? _buttonCommand;
        private string? _buttonText;
        private IDownloadMe Downloader { get; }
        private CancellationTokenSource CancellationSource { get; set; }
        private string? _currentStyleKey;

        #endregion

        #region Bindables

        public int ProgressValue
        {
            get => _progressValue;
            set => SetProperty(ref _progressValue, value);
        }

        public int TotalProgress
        {
            get => _totalProgress;
            set => SetProperty(ref _totalProgress, value);
        }

        public string? State
        {
            get => _state;
            set
            {
                SetProperty(ref _state, value);
                UpdateCommandValue();
            }
        }

        public bool IsIndeterminate
        {
            get => _isIndeterminate;
            set => SetProperty(ref _isIndeterminate, value);
        }

        public string ButtonText
        {
            get => _buttonText;
            set => SetProperty(ref _buttonText, value);
        }


        public string CurrentStyleKey
        {
            get => _currentStyleKey ?? string.Empty;
            set => SetProperty(ref _currentStyleKey, value);
        }

        #endregion

        #region ctor

        public MainWindowModel(IDownloadMe downloader)
        {
            Downloader = downloader;
            CurrentStyleKey = "MainStyle";
            CancellationSource = new CancellationTokenSource();
            ButtonCommand = RestartProgress;
            ButtonText = "START";

            SubscribeToModel();
        }

        #endregion

        #region Helper methods

        private void SubscribeToModel()
        {
            TotalProgress = Downloader.TotalBytesToDownload;

            Downloader.Connecting += () => { State = "Connecting..."; };
            Downloader.Connected += () => { State = "Connected"; };
            Downloader.Paused += () => { State = "Paused"; };
            Downloader.Finishing += () => { State = "Finishing..."; };
            Downloader.Finished += () =>
            {
                ProgressValue = Downloader.TotalBytesToDownload;
                State = "Finished";
            };
            Downloader.BytesReceived += bytes => { ProgressValue += bytes; };
        }

        private void UpdateCommandValue()
        {
            switch (State)
            {
                case "Connecting...":
                case "Finishing...":
                {
                    ButtonCommand = PauseProgress;
                    ButtonText = "PAUSE";
                    IsIndeterminate = true;
                    break;
                }
                case "Connected":
                {
                    ButtonCommand = PauseProgress;
                    ButtonText = "PAUSE";
                    IsIndeterminate = false;
                    break;
                }
                case "Paused":
                {
                    ButtonCommand = ResumeProgress;
                    ButtonText = "RESUME";
                    IsIndeterminate = false;
                    break;
                }
                case "Finished":
                {
                    ButtonCommand = RestartProgress;
                    ButtonText = "RESTART";
                    IsIndeterminate = false;
                    break;
                }
            }
        }

        #endregion

        #region Commands

        public ICommand ButtonCommand
        {
            get => _buttonCommand;
            set => SetProperty(ref _buttonCommand, value);
        }

        private DelegateCommand ResumeProgress =>
            new DelegateCommand(() =>
            {
                new Task(() =>
                    Downloader.StartDownload(CancellationSource.Token, ProgressValue)).Start();
                CancellationSource = new CancellationTokenSource();
            });

        private DelegateCommand PauseProgress =>
            new DelegateCommand(() =>
            {
                CancellationSource.Cancel();
            });
        
        private DelegateCommand RestartProgress =>
            new DelegateCommand(() =>
            {
                ProgressValue = 0;
                
                CancellationSource = new CancellationTokenSource();

                new Task(() =>
                    Downloader.StartDownload(CancellationSource.Token)).Start();
            });

        public DelegateCommand SwitchThemeCommand => new DelegateCommand(
            () =>
                CurrentStyleKey =
                    CurrentStyleKey == "MainStyle"
                        ? "SecondStyle"
                        : "MainStyle");

        #endregion
    }
}
