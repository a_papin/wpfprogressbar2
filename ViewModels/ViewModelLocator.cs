﻿using Autofac;

namespace WpfProgressBar.ViewModels
{
    public class ViewModelLocator
    {
        public MainWindowModel MainWindowModel => App.Container.Resolve<MainWindowModel>();
    }
}
