﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace WpfProgressBar.Common
{
    public class CircularBufferCollection<T> : IEnumerable<T>
    {
        private readonly int _size;
        private readonly object _locker;

        private int _count;
        private int _head;
        private int _rear;
        private readonly T[] _values;

        public CircularBufferCollection(int max)
        {
            _size = max;
            _locker = new object();
            _count = 0;
            _head = 0;
            _rear = 0;
            _values = new T[_size];
        }

        private static int Incr(int index, int size)
        {
            return (index + 1) % size;
        }

        private void UnsafeEnsureQueueNotEmpty()
        {
            if (_count == 0)
                throw new Exception("Empty queue");
        }

        public int Size => _size;

        public object SyncRoot => _locker;

        #region Count

        public int Count => UnsafeCount;

        public int SafeCount
        {
            get
            {
                lock (_locker)
                {
                    return UnsafeCount;
                }
            }
        }

        public int UnsafeCount => _count;

        #endregion

        #region Enqueue

        public void Enqueue(T o)
        {
            UnsafeEnqueue(o);
        }

        public void SafeEnqueue(T o)
        {
            lock (_locker)
            {
                UnsafeEnqueue(o);
            }
        }

        public void UnsafeEnqueue(T o)
        {
            _values[_rear] = o;

            if (Count == Size)
                _head = Incr(_head, Size);
            _rear = Incr(_rear, Size);
            _count = Math.Min(_count + 1, Size);
        }

        #endregion

        #region Dequeue

        public T Dequeue()
        {
            return UnsafeDequeue();
        }

        public T SafeDequeue()
        {
            lock (_locker)
            {
                return UnsafeDequeue();
            }
        }

        public T UnsafeDequeue()
        {
            UnsafeEnsureQueueNotEmpty();

            T res = _values[_head];
            _values[_head] = default;
            _head = Incr(_head, Size);
            _count--;

            return res;
        }

        #endregion

        #region Peek

        public T Peek()
        {
            return UnsafePeek();
        }

        public T SafePeek()
        {
            lock (_locker)
            {
                return UnsafePeek();
            }
        }

        public T UnsafePeek()
        {
            UnsafeEnsureQueueNotEmpty();

            return _values[_head];
        }

        #endregion


        #region GetEnumerator

        public IEnumerator<T> GetEnumerator()
        {
            return UnsafeGetEnumerator();
        }

        public IEnumerator<T> SafeGetEnumerator()
        {
            lock (_locker)
            {
                List<T> res = new List<T>(_count);
                var enumerator = UnsafeGetEnumerator();
                while (enumerator.MoveNext())
                    res.Add(enumerator.Current);
                return res.GetEnumerator();
            }
        }

        public IEnumerator<T> UnsafeGetEnumerator()
        {
            int index = _head;
            for (int i = 0; i < _count; i++)
            {
                yield return _values[index];
                index = Incr(index, _size);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}
