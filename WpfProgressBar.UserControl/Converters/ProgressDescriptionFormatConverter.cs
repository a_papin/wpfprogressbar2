﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using WpfProgressBar.ViewModels;
using WpfProgressBar.Views;

namespace WpfProgressBar.Converters
{
    public class ProgressDescriptionFormatConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (null == values || !values.Any())
                return string.Empty;

            var unitName = values[0] as string;
            var speed = (values[1] as double?)?.ToString(culture);
            var progress = (values[2] as int?)?.ToString(culture);
            var maximum = (values[3] as int?)?.ToString(culture);
            
            if (unitName == "bytes")
            {
                var converter = new UnitsToTextConverter();
                speed = (string)converter.Convert(new[] {values[1], unitName}, targetType, parameter, culture);
                progress = (string)converter.Convert(new[] {values[2], unitName}, targetType, parameter, culture);
                maximum = (string)converter.Convert(new[] {values[3], unitName}, targetType, parameter, culture);
                
                return string.Format(culture, "Downloaded {0} / {1} Speed: {2}/s", progress, maximum, speed);
            }
            
            return string.Format(culture, "{0} / {1} {2} Speed: {3}/s", progress, maximum, unitName ?? "items", speed);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}