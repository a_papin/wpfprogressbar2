﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace WpfProgressBar.Converters
{
    public class ProgressValueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (null == values)
                return 0;

            var controlWidth = values[0] as double?;
            var currentValue = values[1] as int?;
            var minimum = values[2] as int?;
            var maximum = values[3] as int?;

            if (null == controlWidth || controlWidth <= 0)
                return 0;

            if (null == currentValue || null == minimum || null == maximum)
                return 0;

            return controlWidth * currentValue / (maximum - minimum);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
