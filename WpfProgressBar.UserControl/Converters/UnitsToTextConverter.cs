﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Windows.Data;

namespace WpfProgressBar.Converters
{
    public class UnitsToTextConverter : IMultiValueConverter
    {
        private static readonly string[] SizeSuffixes =
                   { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
        [SuppressMessage("ReSharper", "FormatStringProblem")]
        static string SizeSuffix(long value, int decimalPlaces = 1)
        {
            if (decimalPlaces < 0) { throw new ArgumentOutOfRangeException(nameof(decimalPlaces)); }
            if (value < 0) { return "-" + SizeSuffix(-value); }
            if (value == 0) { return string.Format(CultureInfo.CurrentCulture, "{0:n" + decimalPlaces + "} bytes", 0); }

            var mag = (int)Math.Log(value, 1024);
            
            var adjustedSize = (decimal)value / (1L << (mag * 10));

            if (Math.Round(adjustedSize, decimalPlaces) >= 1000)
            {
                mag += 1;
                adjustedSize /= 1024;
            }

            return string.Format(CultureInfo.CurrentCulture, "{0:n" + decimalPlaces + "} {1}",
                adjustedSize,
                SizeSuffixes[mag]);
        }

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values?[0] == null)
                return string.Empty;

            if ((string) values[1] != "bytes")
                return values[0];

            return values[0] switch
            {
                double d => SizeSuffix((long) d),
                int i => SizeSuffix(i),
                _ => string.Empty
            };
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
