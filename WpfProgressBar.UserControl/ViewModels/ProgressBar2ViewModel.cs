﻿using System.Linq;
using System.Timers;
using System.Windows;
using Prism.Mvvm;
using WpfProgressBar.Common;

namespace WpfProgressBar.ViewModels
{
    public class ProgressBar2ViewModel : BindableBase
    {
        #region ProgressBar value properties

        private int _progressValue;

        public int ProgressValue
        {
            get => _progressValue;
            set => SetProperty(ref _progressValue, value);
        }

        private int PrevProgressValue { get; set; }

        #endregion

        #region Speed value calculation

        private Timer? CurrentSpeedCounter { get; set; }

        private CircularBufferCollection<int>? CurrentSpeeds { get; set; }

        public double Speed => CurrentSpeeds.Sum(m => m) / 3d;

        #endregion

        #region ctor

        public ProgressBar2ViewModel()
        {
            ResetCounters();
            SetupTimer();
        }

        #endregion

        #region Model logic

        private void SetupTimer()
        {
            CurrentSpeedCounter = new Timer(1000);
            CurrentSpeedCounter.Elapsed += (o, args) =>
            {
                CurrentSpeeds.SafeEnqueue(ProgressValue - PrevProgressValue);
                PrevProgressValue = ProgressValue;

                RaisePropertyChanged(nameof(Speed));
            };
            Application.Current.Exit += (sender, args) => CurrentSpeedCounter.Dispose();
            
            CurrentSpeedCounter.Start();
        }

        private void ResetCounters() => CurrentSpeeds = new CircularBufferCollection<int>(3);

        #endregion
    }
}