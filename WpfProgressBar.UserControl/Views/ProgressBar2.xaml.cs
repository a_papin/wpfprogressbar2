﻿using Autofac;
using System.Windows;
using System.Windows.Media;
using WpfProgressBar.ViewModels;

namespace WpfProgressBar.Views
{
    /// <summary>
    /// Interaction logic for ProgressBar2.xaml
    /// </summary>
    public partial class ProgressBar2
    {
        #region Bindables

        public ProgressBar2()
        {
            InitializeComponent();

            Model = new ProgressBar2ViewModel();
        }

        public int ProgressValue
        {
            get => (int)GetValue(ProgressValueProperty);
            set => SetValue(ProgressValueProperty, value);
        }

        public int Minimum
        {
            get => (int)GetValue(MinimumProperty);
            set => SetValue(MinimumProperty, value);
        }
        
        public int Maximum
        {
            get => (int)GetValue(MaximumProperty);
            set => SetValue(MaximumProperty, value);
        }

        public Brush FillColor
        {
            get => (Brush)GetValue(FillColorProperty);
            set => SetValue(FillColorProperty, value);
        }
        
        public Brush TextColor
        {
            get => (Brush)GetValue(TextColorProperty);
            set => SetValue(TextColorProperty, value);
        }

        public string UnitName
        {
            get => (string)GetValue(UnitNameProperty);
            set => SetValue(UnitNameProperty, value);
        }
        
        public string Text
        {
            get => (string)GetValue(TextProperty);
            set => SetValue(TextProperty, value);
        }

        public ProgressBar2ViewModel Model
        {
            get => (ProgressBar2ViewModel)GetValue(ModelProperty);
            set => SetValue(ModelProperty, value);
        }

        public bool IsIndeterminate
        {
            get => (bool)GetValue(IsIndeterminateProperty);
            set => SetValue(IsIndeterminateProperty, value);
        }

        #endregion

        #region DependencyProperty definitions

        public static readonly DependencyProperty ProgressValueProperty =
            DependencyProperty.Register("ProgressValue", 
                typeof(int), 
                typeof(ProgressBar2),
                new PropertyMetadata(0,
                    (o, args) =>
                    {
                        ((ProgressBar2) o).Model.ProgressValue = (int) args.NewValue;
                    }));
        public static readonly DependencyProperty MinimumProperty =
            DependencyProperty.Register("Minimum", typeof(int), typeof(ProgressBar2), new PropertyMetadata(0));
        public static readonly DependencyProperty MaximumProperty =
            DependencyProperty.Register("Maximum", typeof(int), typeof(ProgressBar2), new PropertyMetadata(0));
        public static readonly DependencyProperty FillColorProperty =
            DependencyProperty.Register("TextColor", typeof(Brush), typeof(ProgressBar2), new PropertyMetadata(Brushes.Red));
        public static readonly DependencyProperty TextColorProperty =
            DependencyProperty.Register("FillColor", typeof(Brush), typeof(ProgressBar2), new PropertyMetadata(Brushes.Red));
        public static readonly DependencyProperty UnitNameProperty =
            DependencyProperty.Register("UnitName", typeof(string), typeof(ProgressBar2), new PropertyMetadata(string.Empty));
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(ProgressBar2), new PropertyMetadata(string.Empty));
        public static readonly DependencyProperty ModelProperty =
            DependencyProperty.Register("Model", typeof(ProgressBar2ViewModel), typeof(ProgressBar2), new PropertyMetadata(null));
        public static readonly DependencyProperty IsIndeterminateProperty =
            DependencyProperty.Register("IsIndeterminate", typeof(bool), typeof(ProgressBar2), new PropertyMetadata(false));

        #endregion
    }
}
